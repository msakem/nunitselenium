﻿
using System;
using System.Windows.Forms;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.IE;

namespace UnitTestSeleniumP
{
 
    [TestFixture]
    public class UntitledTestCase
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            //driver = new FirefoxDriver();
            //baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
            driver = new InternetExplorerDriver(@"D:\Msakem\driver\IEDriverServer_Win32_3.5.1\");

        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheUntitledTestCaseTest()
        {
            driver.Navigate().GoToUrl("http://nervgh.github.io/pages/angular-file-upload/examples/simple/");
            driver.FindElement(By.XPath("(//input[@type='file'])[2]")).Click();
           // driver.FindElement(By.XPath("(//input[@type='file'])[2]")).Clear();

          SendKeys.SendWait("C:\\fakepath\\aaaa.gif");
            SendKeys.SendWait(@"{Enter}");
            driver.FindElement(By.XPath("(//button[@type='button'])[4]")).Click();
            SendKeys.SendWait("");
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
    
  
}
